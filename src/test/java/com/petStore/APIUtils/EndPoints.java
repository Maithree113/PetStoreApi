package com.petStore.APIUtils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import petstore.domain.Pet;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class EndPoints {

    private RequestSpecification requestSpecification;
    private String queryStatus = "status";
    private String pathPetId = "petId";

    public EndPoints(String baseUrl) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri(baseUrl);
        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecBuilder.addHeader("apiKey","special-key");
        requestSpecification = requestSpecBuilder.build();
    }

    public Pet addNewPet(Pet pet) {
        Response response =  given(requestSpecification)
                .contentType(ContentType.JSON)
                .body(pet)
                .post(Route.postPet())
                .then().extract().response();

        return response.getBody().as(Pet.class);
    }

    public Response getPetsByStatus(String status) {
        return given(requestSpecification)
                .queryParam(queryStatus, status)
                .get(Route.getPetByStatus())
                .then().extract().response();
    }

    public void deletePet(Pet pet) {
        given(requestSpecification)
                .pathParam(pathPetId, pet.getId())
                .delete(Route.deletePet())
                .then().log().all();
    }

    public Response deletePetByInvalidId(String petId) {
        return given(requestSpecification)
                .pathParam(pathPetId, petId)
                .delete(Route.deletePet())
                .then().extract().response();
    }

    public void verifyPetDeleted(Pet pet, String notFound) {
        given(requestSpecification)
                .pathParam(pathPetId, pet.getId())
                .get(Route.deletePet())
                .then().log().all()
                .body(containsString(notFound));
    }

    public Response getPetByInvalidId(String petId) {
        return given(requestSpecification)
                .pathParam(pathPetId, petId)
                .get(Route.getPetById())
                .then().extract().response();
    }

    public Pet findPet(Pet pet) {
        Response response = given(requestSpecification)
                .pathParam(pathPetId, pet.getId())
                .get(Route.getPetById())
                .then().extract().response();

        return response.getBody().as(Pet.class);
    }

    public Response updatePetById(String petId, Pet pet) {
          return given(requestSpecification)
                  .pathParam(pathPetId, petId)
                  .body(pet)
                  .post(Route.updatePetById())
                  .then().extract().response();
    }

    public Pet updatePet(Pet pet) {
        Response response = given(requestSpecification)
                .body(pet)
                .put(Route.postPet())
                .then().extract().response();

        return response.getBody().as(Pet.class);
    }

}
