package com.petStore.APIUtils;

public class Route {

    private static final String PET_ENDPOINT = "/pet";

    public static String postPet() { return PET_ENDPOINT; }

    public static String updatePetById() { return PET_ENDPOINT + "/{petId}"; }

    public static String getPetByStatus() { return PET_ENDPOINT + "/findByStatus" ; }

    public static String getPetById() {return PET_ENDPOINT + "/{petId}";}

    public static String deletePet() { return PET_ENDPOINT + "/{petId}"; }

}
