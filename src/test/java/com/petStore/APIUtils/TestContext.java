package com.petStore.APIUtils;

import com.petStore.dataProvider.ConfigReader;

public class TestContext {

    private ScenarioContext scenarioContext;

    private EndPoints endPoints;

    public TestContext() {

        endPoints = new EndPoints(ConfigReader.getInstance().getBaseUrl());
        scenarioContext = new ScenarioContext(); }

    public EndPoints getEndPoints() {
        return endPoints;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }
}
