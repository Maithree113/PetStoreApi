package com.petStore.stepDef;

import io.cucumber.java.Scenario;
import io.restassured.response.Response;
import com.petStore.APIUtils.EndPoints;
import com.petStore.APIUtils.ScenarioContext;
import com.petStore.APIUtils.TestContext;
import org.apache.commons.lang3.RandomStringUtils;
import petstore.domain.Category;
import petstore.domain.Pet;
import petstore.domain.Status;
import petstore.domain.Tag;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class BaseStep {

    private final ScenarioContext scenarioContext;
    protected Scenario scenario;
    protected Pet petResponse;
    protected Response response;
    private EndPoints endPoints;

    public BaseStep(TestContext testContext)
    {
        endPoints = testContext.getEndPoints();
        scenarioContext = testContext.getScenarioContext();
    }

    public EndPoints getEndPoints() {
        return endPoints;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

    public Pet getRequestBody(String petName, List<String> petUrl, String petBreed, String categoryType, String status)
    {
        Random rd = new Random();
        return new Pet.Builder()
                .withId(RandomStringUtils.randomNumeric(10))
                .withName(petName)
                .withPhotoUrls(petUrl)
                .withStatus(Status.valueOf(status))
                .withTags(Collections.singletonList(new Tag(rd.nextInt(2), petBreed)))
                .inCategory(new Category(rd.nextInt(2), categoryType)).build();
    }
}
