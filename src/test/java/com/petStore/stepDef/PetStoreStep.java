package com.petStore.stepDef;

import com.petStore.APIUtils.TestContext;

import com.petStore.enums.Context;
import io.cucumber.java.Scenario;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import petstore.domain.Pet;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.testng.Assert.assertTrue;

public class PetStoreStep extends BaseStep {

    public PetStoreStep(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("pet name {string} with image {string} belongs to pet category {string} and breed {string}")
    public void pet_name_with_image_belongs_to_pet_category_and_breed(String petName, String petImage, String petCategory, String petBreed) {
        Pet pet = getRequestBody(petName, Collections.singletonList(petImage),
                petCategory, petBreed, "available");
        getScenarioContext().setContext(Context.PET, pet);
        scenario.log("Building pet request body");
    }

    @When("I try to post pet details")
    public void i_try_to_post_pet_details() {
        petResponse =  getEndPoints().addNewPet((Pet) getScenarioContext().getContext(Context.PET));
        scenario.log("New Pet added with Id : " + petResponse.getId());
    }

    @Then("pet details should be submitted")
    public void pet_details_should_be_submitted() {
        assertThat(petResponse, is(samePropertyValuesAs(getScenarioContext().getContext(Context.PET))));
        scenario.log("New Pet added with Name : " + petResponse.getName());
    }
    @When("I try to find pet by id")
    public void i_try_to_find_pet_by_id() {
        petResponse = getEndPoints().findPet((Pet) getScenarioContext().getContext(Context.PET));
        if(petResponse.toString().contains("Pet not found"))
        {
            petResponse = getEndPoints().findPet((Pet) getScenarioContext().getContext(Context.PET));
        }
        scenario.log("Finding Pet by Id : " + petResponse.getId());
    }
    @Then("pet details should be retrieved")
    public void pet_details_should_be_retrieved() {
        assertThat(petResponse, is(samePropertyValuesAs(getScenarioContext().getContext(Context.PET))));
        scenario.log("Pet details with Id : " + petResponse.getId() + "retrieved");
    }
    @When("I try to update pet name {string}")
    public void i_try_to_update_pet_name(String petName) {
        Pet pet = (Pet) getScenarioContext().getContext(Context.PET);
        pet.setName(petName);
        petResponse = getEndPoints().updatePet(pet);
        scenario.log("Pet name updated with : " + petName);
    }
    @Then("pet details should be updated")
    public void pet_details_should_be_updated() {
        assertThat(petResponse, is(samePropertyValuesAs(getScenarioContext().getContext(Context.PET))));
        scenario.log("Pet name is updated : " + petResponse.getName());
    }
    @When("I try to delete pet")
    public void i_try_to_delete_pet() {
        getEndPoints().deletePet((Pet) getScenarioContext().getContext(Context.PET));
        scenario.log("Pet with id : " + petResponse.getId() + "is deleted");
    }

    @Then("pet should be deleted")
    public void pet_should_be_deleted() {
        getEndPoints().verifyPetDeleted((Pet) getScenarioContext().getContext(Context.PET),"Pet not found");
    }

    @Given("pet status is {string}")
    public void pet_status_is(String status) {
        getScenarioContext().setContext(Context.STATUS, status);
        scenario.log("Finding pet by status : " + status);
    }

    @When("I try to get pet details with pet status")
    public void i_try_to_get_pet_details_with_pet_status() {
         response = getEndPoints().getPetsByStatus(getScenarioContext().getContext(Context.STATUS).toString());
         scenario.log("All pet by status : " + getScenarioContext().getContext(Context.STATUS).toString() + "found");
         scenario.log(response.getBody().asString());
    }

    @Given("pet Id is {string}")
    public void pet_id_is(String petId) {
        getScenarioContext().setContext(Context.PET_ID, petId);
    }

    @When("I try to get pet details with invalid pet id")
    public void i_try_to_get_pet_details_with_invalid_pet_id() {
        response = getEndPoints().getPetByInvalidId(getScenarioContext().getContext(Context.PET_ID).toString());
        scenario.log("Finding pet by id : " + getScenarioContext().getContext(Context.PET_ID).toString());
    }

    @When("I try to delete pet details with invalid pet id")
    public void i_try_to_delete_pet_details_with_invalid_pet_id() {
        response = getEndPoints().deletePetByInvalidId(getScenarioContext().getContext(Context.PET_ID).toString());
        scenario.log("Deleting pet by id : " + getScenarioContext().getContext(Context.PET_ID).toString());
    }

    @When("I try to update pet name {string} with invalid petId")
    public void i_try_to_update_pet_name_with_invalid_pet_id(String petName) {
        Pet pet = getRequestBody(null, null,
                null, null, "available");
        getScenarioContext().setContext(Context.PET, pet);
        pet.setName(petName);
        response = getEndPoints().updatePetById(getScenarioContext().getContext(Context.PET_ID).toString(), pet);
        scenario.log("Updating pet by id : " + getScenarioContext().getContext(Context.PET_ID).toString());
    }

    @Then("status {int} with message {string} displayed")
    public void statusWithMessageDisplayed(int status, String message) {
        assertThat(response.getStatusCode(), is(samePropertyValuesAs(status)));
        assertTrue(response.getBody().asString().contains(message));
        scenario.log("finding pet by id : " + getScenarioContext().getContext(Context.PET_ID).toString() + "resulted in " + message);
    }

    @Then("status {int} should be displayed")
    public void statusShouldBeDisplayed(int status) {
        assertThat(response.getStatusCode(), is(samePropertyValuesAs(status)));
        scenario.log("Pet by id : " + getScenarioContext().getContext(Context.PET_ID).toString() + " not found");
    }

    @Then("status should be {int}")
    public void statusShouldBe(int status) {
        assertThat(response.getStatusCode(), is(samePropertyValuesAs(status)));
        scenario.log("All pets with status available retrieved");
    }
}