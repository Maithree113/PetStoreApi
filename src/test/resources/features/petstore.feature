Feature: Petstore API

  @PetWithImage
  Scenario Outline: Post a new available pet to the store.
    Given pet name "<petName>" with image "<petImage>" belongs to pet category "<petCategory>" and breed "<petBreed>"
    When I try to post pet details
    Then pet details should be submitted
    When I try to find pet by id
    Then pet details should be retrieved
    When I try to update pet name "<newPetName>"
    Then pet details should be updated
    When I try to delete pet
    Then pet should be deleted

    Examples:
      |petName |petCategory|petBreed  |newPetName|petImage|
      | lion   | Dog       |labarador |Tommy     |https://mykutya.eoldal.hu/fenykepek/labrador-retriever.-.html|

  @GetPetDetailsByStatus
  Scenario Outline: Get pet details by status
    Given pet status is "<petStatus>"
    When I try to get pet details with pet status
    Then status should be 200

    Examples:
      |petStatus|
      |available|
      |pending  |

  @GetPetDetailsByInvalidId
  Scenario Outline: Get pet details by invalid pet id
    Given pet Id is "<petId>"
    When I try to get pet details with invalid pet id
    Then status 404 with message "<message>" displayed

    Examples:
      |petId|message|
      |24567|Pet not found|


  @DeletePetByInvalidId
  Scenario Outline: Delete pet details by invalid pet id
    Given pet Id is "<petId>"
    When I try to delete pet details with invalid pet id
    Then status 404 should be displayed

    Examples:
      |petId|
      |24567|

  @UpdatePetByInValidDetails
  Scenario Outline: Update pet status by invalid pet id
    Given pet Id is "<petId>"
    When I try to update pet name "<petName>" with invalid petId
    Then status 404 should be displayed

    Examples:
      |petId|petName|
      |24567|Poes1  |

