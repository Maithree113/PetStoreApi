# PetStoreApi

Steps to work with PetStoreApi Project

- Clone the repo using the command git clone 
- Once the repo is cloned
  - run mvn clean install to download dependencies and run the test
  - once tests run cucumber report will be generated 
  
Technology stack used
- Java
- Rest Assured
- Cucumber
- Maven
- TestNG

IDE Used
- IntelliJ

Folder structure

- src/main/java/petstore/domain/ - Contains all pojo classes for building request and response
- src/test/java/com/petStore/stepDef - Contains all the step definitions
- src/test/java/com/petStore/utilities -  Contains all API Utilities needed for the tests like endpoint configuration, routes to resources, test and scenario context.
- src/test/java/com/petStore/config - Contains configuration file which has the base url details
- src/test/java/com/petStore/dataProvider - Contains class to read configuration file
- src/test/java/com/petStore/enums - Contains enums which are leveraged in the step definition to store and pass details between scenarios

- src/test/java - Contains cucumber runner

- test/resources/features - Contains all feature files

- test/resources/cucumber.properties - Contains details to publish cucumber reports and share it among the teams

- test/resources/testRunner.xml - Used in maven to run all tests

Different ways to run the feature files

- Right click on testRunner.xml to run all tests
- Navigate to project directory and use mvn clean install to run all feature files
- Navigate to project directory and use mvn clean verify to run all feature files
- Run using Jenkins job by using command "mvn clean verify"
- Run using GitLab CI pipeline based on 'gitlab-ci.yml' added in the project

- Logging
- Scenario.log is used to log all step interactions into the cucumber report (target/cucumber-html-reports/cucumber-html-reports/overview-features.html)

Report with logging image
<img src="src/main/java/image/ReportWithLogging.png"/>

Reports
- Cucumber report is published at the end of the run can be accessed using this link : https://reports.cucumber.io/reports/8ebfdbd2-ea38-4bd1-a6cb-1564d2b3da8d
- Cucumber html reports are generated under target/cucumber-html-reports - Contains complete details about the project.

Cucumber Report
<img src="src/main/java/image/CucumberReport.png"/>

- CI/CD 
- Url - https://gitlab.com/Maithree113/PetStoreApi
- Cucumber html reports are generated under target/cucumber-html-reports - Contains complete details about the project.
- Navigate to target/cucumber-html-reports/cucumber-html-reports/overview-features.html - To see the reports with all the logging
- Navigate to https://maithree113.gitlab.io/-/PetStoreApi/-/jobs/{job-id}/artifacts/public/overview-features.html/ to see cucumber report



